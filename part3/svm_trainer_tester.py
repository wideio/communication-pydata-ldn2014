## Tutorial file provided by WIDE IO CONSULTING LTD

#svm trainer
from bow import BOW
from featurecomputer import VisionModel
import datasets as ds
import numpy as np
import cv2

default_svm_param={"kernel_type":cv2.SVM_RBF, # CHI2
                      "svm_type":cv2.SVM_C_SVC,
			"gamma":0.3,
                     "C": 0.5 }

def drawgrid(mink,maxk,mine,maxe,res):
	import matplotlib.pyplot as plt
	fig, ax = plt.subplots()
	zipres=zip(*res)
	k=list(zipres[0])
	e=list(zipres[1])
	c=list(zipres[2])				
	ax.set_ylim(mink-100,maxk+100)
	ax.set_xlim(mine-100,maxe+100)
	ax.set_xlabel('Maximum eigenvectors')
	ax.set_ylabel('K')
	#lighter=better
	plt.scatter(e, k, c=c, s=500)
	plt.gray()
	print res
	plt.show()

def parambash():
	import datasets as ds
	paths=ds.get_paths("dataset/pos")
	#kgrid=[2**i for i in range(7,9)]
	#egrid=[i for i in range(72,120,24)]
	kgrid=[2**i for i in range(7,11)]
	egrid=[i for i in range(48,168,24)]
	mink=kgrid[0]
	maxk=kgrid[len(kgrid)-1]
	mine=egrid[0]
	maxe=egrid[len(egrid)-1]
	resultsgrid=[]
	ctr=0
	for k in kgrid:
		for e in egrid:
			newmodel=VisionModel(detector="FAST",descriptor="SIFT")
			newmodel.train(paths=paths,maxComponents=e,K=k)
			resultsgrid.append(train_svm_with_model(newmodel,default_svm_param,ctr))
			ctr+=1
	#return resultsgrid
	drawgrid(mink,maxk,mine,maxe,resultsgrid)
			

def train_svm_with_model(model,params,idd):
	#get training/testing sets
	train_set,train_set_labels,test_set,test_set_labels=ds.generate_sets(["dataset/pos"],["dataset/neg"],["dataset/pos2"],["dataset/neg2"],10,10)
	#stack train data
	trainData= model.StackFeaturesFromPaths(train_set)
	svm=cv2.SVM()
	#setup search grids
	k_fold=10
	cgrid=np.linspace(2**-8,2**8,17)
	ggrid=np.linspace(2**-8,2**8,17)
	svm.train_auto(trainData,train_set_labels,None,None,params = params,k_fold=k_fold,Cgrid=cgrid,gammaGrid=ggrid)

	#stack test data
	testData=model.StackFeaturesFromPaths(test_set)
	#predict all test using svm
	svm_labels=svm.predict_all(testData)
	sucrate=float(sum(not not (svm_labels[i]==test_set_labels[i])for i in range(len(test_set_labels)))*100)/len(test_set_labels)
	print "Success rate: ",sucrate

	#savesvm
	svm.save(ds.CreateDirectory(ds.optPath % (idd))+"svm")
	#generate test report
	ds.GenerateTestReport((ds.optPath % (idd))+"REPORT.HTML",test_set,svm_labels,test_set_labels)
	#save model
	model.save((ds.optPath % (idd)+str(idd)))
	
	return [model.K,model.maxComponents,sucrate]
