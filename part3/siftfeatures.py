## Tutorial file provided by WIDE IO CONSULTING LTD

import cv2
import numpy as np

#load an image
def LoadImage(filepath,colour=False):
	if colour==False:
		return cv2.imread(filepath,0)
	else:
		return cv2.imread(filepath,1)

def img_bgr2bw(img):
	return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

def create_detector(name='FAST'):
	return cv2.FeatureDetector_create(name)

def create_descriptor(name='SIFT'):
	return cv2.DescriptorExtractor_create(name)


def detect_and_compute(detector,descriptor,img_gray):
	# Detect and compute keypints
	kps = detector.detect(img_gray)
	return descriptor.compute(img_gray, kps) #kps, dscs


def demo(filename="dataset/pos/0a1d7c32a4f0dd69f774e8ec028ec754.jpg"):
	#load image
	img_gray=LoadImage(filename)
	desc=create_descriptor()
	det=create_detector()
	kps,dscs=detect_and_compute(det,desc,img_gray)
	import pylab
	img_out = cv2.drawKeypoints(img_gray, kps)
	pylab.imshow(img_out)
	pylab.show()

