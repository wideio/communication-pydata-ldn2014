## Example provided by WIDE IO CONSULTING LTD  -- code based on pycvf implementation

import os
import sys
from PySide import QtCore
from PySide import QtGui

if not (os.environ.has_key("DISABLE_PYQT_CENTRAL_APP")):
  qapp = QtGui.QApplication(sys.argv)
  qapp.processEvents()
else:
  qapp=None

import numpy
from pylab import cm

try:
    LazyDisplayQt__imgconvarray={
                      1:QtGui.QImage.Format_Indexed8,
                      3:QtGui.QImage.Format_RGB888,
                      4:QtGui.QImage.Format_RGB32
                      }
except:
    LazyDisplayQt__imgconvarray={
                      1:QtGui.QImage.Format_Indexed8,
                      4:QtGui.QImage.Format_RGB32
                      }


FULLSCREEN=False

class Display(QtGui.QMainWindow):
        imgconvarray=LazyDisplayQt__imgconvarray
        def __init__(self, *args):
            QtGui.QMainWindow.__init__(self, *args)
            self._i=numpy.zeros((1,1,4),dtype=numpy.uint8)
            self.i=QtGui.QImage(self._i.data,self._i.shape[1],self._i.shape[0],self.imgconvarray[self._i.shape[2]])
            self.colortable=[ (numpy.array(cm.jet(x)[0:3])*256.).astype(numpy.uint8) for x in range(0,256) ]
            if (FULLSCREEN):
               self.set_fullscreen()
            self.show()
            self.setFocus()
        def set_fullscreen(self):
            self.setWindowFlags( QtCore.Qt.FramelessWindowHint | QtCore.Qt.Tool | QtCore.Qt.WindowStaysOnTopHint )
            self.resize( QtGui.QApplication.desktop().size() )
            #self.setFocusPolicy( QtGui.StrongFocus )
            self.setAttribute(QtCore.Qt.WA_QuitOnClose, True)
        def set_colormap(self,cma):
            self.colortable=cma
        def __del__(self):
            self.hide()
        def f(self,thearray):
            self._i=thearray.astype(numpy.uint8).copy('C')
            self.i=QtGui.QImage(self._i.data,self._i.shape[1],self._i.shape[0],self.imgconvarray[self._i.shape[2]])
            if (self._i.shape[2]==1):
                self.i.setColorTable(self.colortable)
            self.update()
            qapp.processEvents()
        def push(self,stamped_img):
         """ We ignore the timestamp and directly display the image on the screen"""
         self.f(stamped_img[0])			
        def paintEvent(self, ev):
            self.p = QtGui.QPainter()
            self.p.begin(self)
            self.p.drawImage(QtCore.QRect(0,0,self.width(),self.height()),
                             self.i,
                             QtCore.QRect(0,0,self.i.width(),self.i.height()))
            self.p.end()

if __name__=="__main__":
  from vio import player_pil as player  
  p=player.Player("akiyo/i_%04d.jpg")
  d=Display()
  for f in p:
    d.push((f,1))        
    