## Example provided by WIDE IO CONSULTING LTD
import cv2
import numpy as np

class Player(object):
  def __init__(self,filename):
    self.cap = cv2.VideoCapture(filename)
    
  def __iter__(self):    
    ret=True
    while ret: 
      ret, rgb = self.cap.read()
      yield rgb
      
  def __del__(self):
     self.cap.release()
