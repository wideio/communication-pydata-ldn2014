## Example provided by WIDE IO CONSULTING LTD
from PIL import Image
import numpy

class Player(object):
  def __init__(self,filename):
    self.seqbase = filename
    
  def __iter__(self):    
    i=1
    while True: 	  
      img=numpy.asarray(Image.open(self.seqbase%(i,)))
      yield img
      i+=1
      