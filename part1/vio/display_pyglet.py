## Example provided by WIDE IO CONSULTING LTD  -- code based on pycvf implementation

import pyglet
from pyglet import font
from pyglet.text import Label

from pyglet.gl import *
glEnable(GL_TEXTURE_2D)
import numpy

FULLSCREEN=False

class Display(object):
    def __init__(self,title="Python Experiment",fullscreen=FULLSCREEN,geometry=None):
        self.window = pyglet.window.Window(fullscreen=fullscreen,vsync=False)
        self.fullscreen=fullscreen
        self.event_loop = pyglet.app.EventLoop()
        self.on_draw=self.window.event(self.on_draw)
        self.on_window_close=self.event_loop.event(self.on_window_close)
        self.ip=None
        self.geometry=geometry
        self.pw=1
        self.ph=1
    def on_window_close(self,window):
        self.event_loop.exit()
        return pyglet.event.EVENT_HANDLED
    def __del__(self):
            self.window.close()
            self.ev_dispatch()
    def f(self,img):
            """ this function updates the image on the screen (this function does a copy of the image)"""
            (h,w,c)=numpy.shape(img)
            if (not self.fullscreen):
              if (w!=self.pw) or(h!=self.ph):
                if (self.geometry!=None):
                  self.pw,self.ph=self.geometry
                else:
                  self.pw=w
                  self.ph=h
                self.window.set_size(self.pw,self.ph)
                self.ev_dispatch()
                self.ev_dispatch()
                self.ev_dispatch()
            if (c==3):
                self.ip=pyglet.image.ImageData(w,h,'BGR',img.astype(numpy.uint8).tostring())#[0])
            elif (c==4):
                self.ip=pyglet.image.ImageData(w,h,'BGRA',img.astype(numpy.uint8).tostring())#[0])
            else:
                raise Exception,"unsupported number of channels"
            self.ev_dispatch()
    def push(self,stamped_img):
         """ We ignore the timestamp and directly display the image on the screen"""
         self.f(stamped_img[0])
    def on_draw(self):
        """ this is the event handler that draws the window"""
        try:
            if (not self.ip):
                print "No Image"
                return
            self.window.clear()
            glPushMatrix()
            glTranslatef(0,self.window.height,0)
            glScalef(float(self.window.width)/self.ip.width,-(float(self.window.height)/self.ip.height),1)
            self.ip.blit(0,0)
            glPopMatrix()
            #print "."
        except Exception,e:
            print str(e)
            pass
    def ev_dispatch(self):
         """ this is an element of the event handler loop """
         pyglet.clock.tick()
         for window in pyglet.app.windows:
             window.switch_to()
             window.dispatch_events()
             window.dispatch_event('on_draw')
             window.flip()

    
if __name__=="__main__":
  from vio import player_cv as player  
  p=player.Player("akiyo/i_%04d.jpg")
  d=Display()
  for f in p:
    d.push((f,1))        
    